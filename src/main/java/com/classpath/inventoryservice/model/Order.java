package com.classpath.inventoryservice.model;


import lombok.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;


@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "orderId")
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Order {

    private long orderId;

   
    private double totalPrice;

    
    private LocalDate orderDate;

   
    private Set<OrderLineItem> lineItems = new HashSet<>();

    //scaffolding method to set the bidirectional mapping
    public void addOrderLineItem(OrderLineItem orderLineItem){
        orderLineItem.setOrder(this);
        this.lineItems.add(orderLineItem);
    }

}